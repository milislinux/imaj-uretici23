#!/usr/bin/env python

import json
import subprocess
import sys, os

from pathlib import Path
home = str(Path.home())

WEATHER_CODES = {
    '113': '',
    '116': '',
    '119': '',
    '122': '️',
    '143': '',
    '176': '',
    '179': '',
    '182': '',
    '185': '',
    '200': '',
    '227': '',
    '230': '',
    '248': '',
    '260': '',
    '263': '',
    '266': '',
    '281': '',
    '284': '',
    '293': '',
    '296': '',
    '299': '',
    '302': '',
    '305': '',
    '308': '',
    '311': '',
    '314': '',
    '317': '',
    '320': '',
    '323': '',
    '326': '',
    '329': '️',
    '332': '',
    '335': '️',
    '338': '',
    '350': '',
    '353': '',
    '356': '',
    '359': '',
    '362': '',
    '365': '',
    '368': '',
    '371': '️️',
    '374': '',
    '377': '',
    '386': '',
    '389': '',
    '392': '',
    '395': '️'
}

get_city = "dinit read localization.place"
location = subprocess.check_output(get_city, shell=True).strip()

if not location or location == "":
	location = "ankara"
else:
	location = location.decode()
	
komut = "curl -s https://tr.wttr.in/{}?format=j1".format(location)

result = subprocess.check_output(komut, shell=True).strip()
data={}

try:
  weather = json.loads(result)

  temp = weather["current_condition"][0]["temp_C"]
  code = weather["current_condition"][0]["weatherCode"]
  stat = weather["current_condition"][0]["lang_tr"][0]["value"]

  data["text"] = "{} {}°C".format(WEATHER_CODES[code], temp)
except:
  data["text"] = "error"

data["tooltip"] = location

print(json.dumps(data))
