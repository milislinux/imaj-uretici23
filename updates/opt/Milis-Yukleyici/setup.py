#!/usr/bin/python3

from setuptools import setup, find_packages
import os

datas = [("/usr/share/applications",["data/milis-kur.desktop"]),
		("/usr/share/icons/hicolor/scalable/apps",["resimler/milis-kur.svg"]),
		("/usr/share/milis-kur-gui/betikler",["betikler/ckbcomp"])]

resimler_liste = []
for resim in os.listdir("resimler"):
	yol = "resimler/{}".format(resim)
	if os.path.isfile(yol):
		resimler_liste.append(yol)
datas.append(("/usr/share/milis-kur-gui/resimler/",resimler_liste))

setup(
	name = "milis-kur",
	scripts = ["milis-kur-gui"],
	packages = find_packages(),
	version = "0.9",
	description = "Milis Installer GTK Frontend",
	author = ["Fatih Kaya"],
	author_email = "sonakinci41@gmail.com",
	url = "https://mls.akdeniz.edu.tr/git/milislinux/Milis-Yukleyici",
	data_files = datas
)
