from gi.repository import Gtk, Gdk, GdkPixbuf
from kutuphaneler import diller
import subprocess, os

class DiskSecim(Gtk.Grid):
	def __init__(self,ebeveyn):
		Gtk.Window.__init__(self)
		self.ebeveyn = ebeveyn
		self.baslik = "Disk Secimi"
		self.ad = "Disk Secimi"
		self.set_column_spacing(5)
		self.set_row_spacing(5)
		self.diskler_liste = {}
		self.disk_secim_combo = []
		self.oto_ata = True
		

		self.diskler_yazi = Gtk.Label()
		self.attach(self.diskler_yazi,0,1,1,1)

		self.disk_yenile = Gtk.Button()
		self.disk_yenile.connect("clicked", self.disk_doldur)
		self.disk_yenile.set_always_show_image(True)
		self.disk_yenile.set_image(Gtk.Image(stock=Gtk.STOCK_REFRESH))
		self.attach(self.disk_yenile,1,1,1,1)
		self.set_baseline_row(2)

		self.diskler_combo = Gtk.ComboBoxText()
		self.diskler_combo.connect("changed", self.disk_secildi)
		self.attach(self.diskler_combo,2,1,1,1)

		sep = Gtk.Separator()
		self.attach(sep,0,2,3,1)

		self.grub_kur_radio = Gtk.RadioButton.new_with_label_from_widget(None,"")
		self.grub_kur_radio.connect("toggled", self.grub_kur_degisti, "kur")
		self.attach(self.grub_kur_radio,0,6,2,1)

		self.grub_combo = Gtk.ComboBoxText()
		self.grub_combo.connect("changed", self.grub_combo_degisti)
		self.attach(self.grub_combo,2,6,2,1)

		self.grub_kurma_radio = Gtk.RadioButton.new_with_label_from_widget(self.grub_kur_radio,"")
		self.grub_kurma_radio.connect("toggled", self.grub_kur_degisti, "kurma")
		self.attach(self.grub_kurma_radio,0,7,3,1)

		sep = Gtk.Separator()
		self.attach(sep,0,5,3,1)

		self.diski_sil = Gtk.RadioButton.new_with_label_from_widget(None,"")
		self.diski_sil.connect("toggled", self.diski_sil_degisti, "diski_sil")
		self.attach(self.diski_sil,0,3,3,1)

		self.baska_birsey = Gtk.RadioButton.new_with_label_from_widget(self.diski_sil,"")
		self.baska_birsey.connect("toggled", self.diski_sil_degisti, "baska_birsey")
		self.attach(self.baska_birsey,0,4,3,1)

		self.disk_doldur(None)

	def disk_duzenle_surec(self,widget):
		surec = subprocess.Popen(['gparted'])
		surec.wait()
		self.disk_doldur(None)

	def disk_doldur(self,widget):
		#Tekrar disk doldurunca oto atama tekrar yapılacak
		self.ebeveyn.milis_ayarlari["takas_disk"] = ""
		self.ebeveyn.milis_ayarlari["sistem_disk"] = ""
		self.ebeveyn.milis_ayarlari["uefi_disk"] = ""
		self.diskler_combo.remove_all()
		self.grub_combo.remove_all()


		self.diskler = {}
		process = subprocess.check_output(["lsblk","-P","-no","PATH,SIZE,FSTYPE,TYPE,PARTFLAGS,MODEL"], timeout=4)
		out = process.decode("UTF-8").rstrip()
		out = out.split("\n")

		disk_sayac = 0
		grub_sayac = 0
		sayac = 0
		for i in out:
			k = i.split()
			path = k[0].replace("PATH=","").replace('"','')
			size = k[1].replace("SIZE=","").replace('"','')
			fstype = k[2].replace("FSTYPE=","").replace('"','')
			type_ = k[3].replace("TYPE=","").replace('"','')
			flags = k[4].replace("PARTFLAGS=","").replace('"','')
			model = k[5].replace("MODEL=","").replace('"','')
			if type_ == "disk" and "/dev/fd" not in path:
				self.diskler[path] = {"SIZE":size, "MODEL":model, "PARTITIONS":[]}
				if path == self.ebeveyn.milis_ayarlari["sistem_disk"]:
					disk_sayac = sayac
				if path == self.ebeveyn.milis_ayarlari["grub_kur"]:
					grub_sayac = sayac
				self.diskler_combo.append_text(path+" | "+model+" | "+size)
				self.disk_secim_combo.append(path+" | "+model+" | "+size)
				self.grub_combo.append_text(path+" | "+model+" | "+size)
				sayac += 1
			elif type_ == "part":
				for key in self.diskler.keys():
					if key in path:
						self.diskler[key]["PARTITIONS"].append([path,size,fstype,type_,flags])
		self.diskler_combo.set_active(disk_sayac)
		self.grub_combo.set_active(grub_sayac)

	def disk_secildi(self,widget):
		disk = self.diskler_combo.get_active_text()
		if disk != None:
			index = self.disk_secim_combo.index(disk)
			self.grub_combo.set_active(index)

	def grub_kur_degisti(self,widget, islem):
		if self.grub_kur_radio.get_active():
			disk = self.grub_combo.get_active_text()
			disk = disk.split(" | ")
			disk = disk[0]
			self.ebeveyn.milis_ayarlari["grub_kur"] = disk
			self.grub_combo.set_sensitive(True)
		elif self.grub_kurma_radio.get_active():
			self.ebeveyn.milis_ayarlari["grub_kur"] = ""
			self.grub_combo.set_sensitive(False)

	def diski_sil_degisti(self,widget, islem):
		if self.diski_sil.get_active():
			self.grub_combo.set_sensitive(True)
		elif self.grub_kurma_radio.get_active():
			self.grub_combo.set_sensitive(False)

	def grub_combo_degisti(self,widget):
		if self.grub_kur_radio.get_active():
			disk = widget.get_active_text()
			if disk != None:
				disk = disk.split(" | ")
				disk = disk[0]
				self.ebeveyn.milis_ayarlari["grub_kur"] = disk

	def dil_ata(self,dil):
		self.dil = dil
		self.baslik = diller.diller[dil]["t29"]
		self.diskler_yazi.set_text(diller.diller[dil]["t83"])
		self.disk_yenile.set_label(diller.diller[dil]["t32"])
		self.grub_kur_radio.set_label(diller.diller[dil]["t37"])
		self.grub_kurma_radio.set_label(diller.diller[dil]["t38"])
		self.diski_sil.set_label(diller.diller[dil]["t84"])
		self.baska_birsey.set_label(diller.diller[dil]["t85"])
