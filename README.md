### Milis Linux 2.3 Hızlı İmaj Oluşturucu ###

1- Sistem dizini için MSYS değişkeni ayarlanır.

```
./iso_olustur.sh $MSYS
```

2- Updates dizini değişkenleri ile özelleştirilmiş imaj için

```
./hizli_iso.sh $MSYS
```
