#!/bin/bash

hata_olustu(){
        echo "$1"
        exit 1
}

# gerekler
[ ! -f /usr/bin/xorriso ] && hata_olustu "xorriso komutu bulunamadı!"
[ ! -f /usr/sbin/mke2fs ] && hata_olustu "mke2fs komutu bulunamadı!"
[ ! -f /usr/bin/mksquashfs ] && hata_olustu "mksquashfs komutu bulunamadı!"
[ ! -f /usr/sbin/mkfs.vfat ] && hata_olustu "mkfs.vfat komutu bulunamadı!"

if [ -z $ROOTFS ];then
	ROOTFS="/home/mlfs/onsistem"
fi

if [ ! -z $1 ];then
	ROOTFS="$1"
fi

if [ ! -d $ROOTFS ];then
	echo "$ROOTFS dizini mevcut değil!"
	exit 1
fi

# iso dizini içine updates dizini oluşturularak ilgili kök dizin altı dizin ve dosyalar konuşlandırılacak.
echo "Sıkıştırma yapılmadan Iso dosyası hazırlanıyor..."
#cp $ROOTFS/usr/lib/syslinux/isohdpfx.bin iso/boot/isolinux/isohdpfx.bin

month="$(date -d "$D" '+%m')"
day="$(date -d "$D" '+%d')"
year="$(date -d "$D" '+%Y')"

# updates dizini ile özel ayarların eklenmesi/üstüne yazılması
rm -rf ./iso/updates
cp -rf updates ./iso/

# minimal imajda görsel kurucunun silinmesi
if [ ! -f $ROOTFS/usr/bin/wayland-scanner ];then
	rm -rf iso/updates/opt/Milis-Yukleyici
	rm -rf iso/updates/root/Masaüstü
fi

echo "Efi ayarları yapılıyor..."
mkdir -p iso/efi_tmp
dd if=/dev/zero bs=1M count=256 of=./iso/efiboot.img
mkfs.vfat -n Milis_EFI ./iso/efiboot.img 

mount -o loop ./iso/efiboot.img ./iso/efi_tmp
cp -rf ./iso/boot/kernel ./iso/efi_tmp/
cp -rf ./iso/boot/initrd ./iso/efi_tmp/
cp -rf ./efi/* ./iso/efi_tmp/

umount ./iso/efi_tmp 
rm -rf ./iso/efi_tmp

echo "milis-2.3-${year}.${month}.${day}" > ./iso/updates/etc/milis-surum
echo "Iso dosyası hazırlanıyor..."
#cp $ROOTFS/usr/lib/syslinux/isohdpfx.bin iso/boot/isolinux/isohdpfx.bin
xorriso -as mkisofs \
-iso-level 3 -rock -joliet \
-max-iso9660-filenames -omit-period \
-omit-version-number -relaxed-filenames -allow-lowercase \
-volid "MILIS_CALISAN" \
-eltorito-boot boot/isolinux/isolinux.bin \
-eltorito-catalog boot/isolinux/isolinux.cat \
-no-emul-boot -boot-load-size 4 -boot-info-table \
-eltorito-alt-boot -e efiboot.img -isohybrid-gpt-basdat -no-emul-boot \
-isohybrid-mbr iso/boot/isolinux/isohdpfx.bin \
-output "milis-2.3-${year}.${month}.${day}.iso" iso || echo "ISO imaj olusturalamadı";

chmod 777 milis-2.3-${year}.${month}.${day}.iso
